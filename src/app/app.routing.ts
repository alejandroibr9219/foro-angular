//Import Necesarios 
import { ModuleWithProviders  } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


//IMPORTAR COMPONENTES
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';


//Definir las Rutas

const appRoutes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'inicio', component:HomeComponent},
	{path: 'login', component: LoginComponent},
	{path: 'registro', component: RegisterComponent},
	{path: '**', component: LoginComponent}
	];


//Exportar Configuracion
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);