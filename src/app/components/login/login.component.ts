import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {

	public page_title: string;
	public user: User;
	public status: string;
	public token;
	public identity;

  constructor(
  	private _userService: UserService,
  	private _router: Router,
  	private _route: ActivatedRoute
  	) { 
  	this.page_title = 'identificate';
  	this.user = new User('', '', '', '', '', '', 'ROLE_USER');

  }

  ngOnInit(): void {
  	//this.logout();
  }


  onSubmit(form){

  	this._userService.signup(this.user).subscribe(
      response => {

        if(response.user && response.user._id){

           this.identity = response.user;
           localStorage.setItem('identity', JSON.stringify(this.identity));
           
       this._userService.signup(this.user, true).subscribe(
      response =>{

      		if(response.token){

      			//PErsistir datos
      		
      		this.token = response.token;

      		localStorage.setItem('token', this.token);
      		
      		
      			//Redireccion a inicio
      		this.status = 'success';	
  			this._router.navigate(['inicio']);
      		}else{

      			this.status = 'error';
      		}


			  
      			},
			   error => {
			        this.status = 'error';
			        console.log(error);

      });

        }else{
          this.status = 'error';
        }
        form.reset();
        
     
      },
      error => {
        this.status = 'error';
        console.log(<any>error);
      }

    );
  }

  /*logout(){

  		this._route.params.subscribe(params => {
  			let logout = +params['sure'];

  			if(logout ==1){
  				localStorage.removeItem('identity');
  				localStorage.removeItem('token');

  				this.identity = null;
  				this.token = null;

  				//Redireccion a inicio

  				this._router.navigate(['inicio']);



  			}

  		});

  }*/

}
